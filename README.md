# Titre du projet
MyResto
## Auteur(s) du projet
Anthony Ranuzzi | Leo Monterrat
## Auteur(s) du ReadMe
Vincent Creusy | Dorian Dragoni
## Résumé du projet
On cherche à réserver un restaurent à proximité et de manger directement en sélectionnant son menu !
## Description de l'application
Cette application permet en 1 clic de réserver ses plats à distance et d'arriver en mangeant sans attendre.
Elle propose un accueil et un menu permettant de naviguer entre plusieurs pages :
- Menu / restauration
- À propos
- Une page énigmatique
- Réservation sous la section communication
## Impressions écrans
Image de l'accueil

![accueil](/uploads/953061632398e669aa512ba4b30dbf8d/accueil.png)

L'application propose un menu latéral

![menu](/uploads/615323eb7b625b3cc7fa85ee0a2f9491/menu.png)

Il y a également une liste qui correspond à un adapter

![menu_restauration](/uploads/ddb11bc38831062d119833e441169706/menu_restauration.png)
## Remarque d’ensemble sur le code
Le code est ordonné, néanmois il reste du code commenté, des mauvaise indentations et des espaces inutiles entre les fonctions.
Il reste également des fonctions d'affichage dans la console (tel que la fonction info), bien que cela permette indirectement de montrer une fonction d'anko.
On aurait également apprécié voir les fonctions et les classes commentées afin de comprendre éventuellement leur utilité.