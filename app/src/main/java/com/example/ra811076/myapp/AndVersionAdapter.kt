package com.example.ra811076.myapp

import android.support.annotation.LayoutRes
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_my_object.view.*
import java.text.FieldPosition

class AndVersionAdapter (private val items: Array<MyObject>) : RecyclerView.Adapter<AndVersionAdapter.ViewHolder>() {



    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindMyObject(items[position])

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) : AndVersionAdapter.ViewHolder {
        return ViewHolder(parent.inflate(R.layout.item_my_object))
    }

    override fun getItemCount(): Int = items.size

    private fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false) : View {
        return android.view.LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
    }

    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        fun bindMyObject(myObject: MyObject) {
            with(myObject) {
                itemView.menuName.text = menu
                itemView.menuImg.setImageResource(img)
                itemView.menuDesc.text = " ${view.resources.getString(R.string.txt_desc)} $desc"
            }
        }
    }
}