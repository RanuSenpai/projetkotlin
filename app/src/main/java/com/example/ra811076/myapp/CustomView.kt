package com.example.ra811076.myapp
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View


class CustomView : View, View.OnTouchListener {

    private var mPaintBlue = Paint()
    private var mPaintRed = Paint()
    private var mPaintGreen = Paint()
    lateinit var mCircle1 : MagicCircle
    lateinit var mCircle2 : MagicCircle


    constructor(context: Context?) : this(context, null)

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs){
        init()
    }

    companion object {
        val DELTA = 8
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        //Mouvement
        mCircle1.move1()
        mCircle2.move2()


        //DrawCircle
        canvas?.drawCircle(mCircle1.cx,mCircle1.cy,mCircle1.rad, mPaintBlue)
        canvas?.drawCircle(mCircle2.cx,mCircle2.cy,mCircle2.rad, mPaintRed)

        invalidate()
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)

        //LayoutCircle
        mCircle1 = MagicCircle(50F,150F,40F ,width.toFloat(), height.toFloat())
        mCircle2 = MagicCircle (550F,1500F,40F,width.toFloat(),height.toFloat())

    }

    private fun init() {
        super.setOnTouchListener(this)
        mPaintBlue.color = Color.BLUE
        mPaintRed.color = Color.RED
        mPaintGreen.color = Color.GREEN
    }

    override fun onTouch(v: View?, motionEvent: MotionEvent): Boolean {
        mCircle2.cx = motionEvent.x
        mCircle2.cy = motionEvent.y
        return true
    }
}