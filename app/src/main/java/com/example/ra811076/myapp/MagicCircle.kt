package com.example.ra811076.myapp

data class MagicCircle(var cx: Float , var cy:Float, var rad:Float, val maxX: Float, val maxY : Float ) {

    var delta = 10
    var deltaRad = 1F
    var dx = delta
    var dy = delta
    var r = deltaRad

    fun move1(){
        when {
            cx < 0F -> dx = delta
            cx > maxX -> dx = -delta
            /*cy < 0F -> dy = delta
            cy > maxY -> dy = -delta*/
        }
        cx += dx
        //cy +=dy
    }

    fun move2() {
         when {
             rad < 0F -> r = deltaRad
             rad > 100F -> r = -deltaRad
         }
        rad += r


    }





}