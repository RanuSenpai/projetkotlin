package com.example.ra811076.myapp


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_reservation.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import org.jetbrains.anko.toast
import java.util.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class ReservationFragment : Fragment(), AnkoLogger {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        info("ON CREATE")
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_reservation, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        nbPicker.maxValue = 100
        nbPicker.minValue = 0
        nbPicker.value = 1
        btnValiderResa.setOnClickListener { createReservation() }
        super.onViewCreated(view, savedInstanceState)
    }

    fun createReservation () {
        activity!!.toast("Reservation : "+ nbPicker.value + " | Reservation n° " + (1..40).random() )
    }

    fun IntRange.random() =
            Random().nextInt((endInclusive + 1) - start) +  start

    companion object {
         var BUNDLE_VAL_PICKER = "BUNDLE_VAL_PICKER"
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        info("ON SAVE")

        outState.putInt(BUNDLE_VAL_PICKER, nbPicker.value)

    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        info("ON RESTORE")
        if (savedInstanceState != null) {
            nbPicker.value = savedInstanceState.getInt(BUNDLE_VAL_PICKER)
        }
    }




}
