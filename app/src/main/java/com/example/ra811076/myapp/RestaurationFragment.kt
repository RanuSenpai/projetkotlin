package com.example.ra811076.myapp


import android.content.Context
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_restauration.*
import org.jetbrains.anko.AnkoLogger


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class RestaurationFragment : Fragment(), AnkoLogger {

     var items = Array<MyObject>(3 ,{ MyObject("?",0,"Bientôt")})

    override fun onActivityCreated(savedInstanceState: Bundle?) {

        super.onActivityCreated(savedInstanceState)

        seedItems()

        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = AndVersionAdapter(items)


    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_restauration, container, false)
    }

     fun seedItems() {
        val tabMenuResto = resources.getStringArray(R.array.menu)
         val tabImg = arrayOf(R.drawable.salade,R.drawable.lasagne,R.drawable.cafe )
         val tabDesc = resources.getStringArray(R.array.desc)
        for (k in 0..(tabMenuResto.size - 1)) {
            items[k] = MyObject(tabMenuResto[k],tabImg[k], tabDesc[k])
        }
    }


}
