package com.example.ra811076.myapp


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button

import kotlinx.android.synthetic.main.fragment_secret.*
import org.jetbrains.anko.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class SecretFragment : Fragment(), AnkoLogger   {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {


        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_secret, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        btnSecret.setOnClickListener { checkPassWord()  }
    }


    private fun checkPassWord () {
        var pwd = etxtSecret.text.toString()
        if (pwd == "11854") {
            activity!!.alert("Tu as gagner un repas offert par le resto de ton choix | code : 123soleil") {
                title = "WINNER !"

            }.show()
        } else {
            activity!!.alert("Tu as perdu, tu dois un repas a Ranuzzi Anthony ! | call 0698288713") {
                title = "LOOSER :("

            }.show()
        }
    }
}
